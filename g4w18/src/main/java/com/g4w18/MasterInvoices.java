/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.g4w18;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author 1331680
 */
@Entity
@Table(name = "master_invoices", catalog = "bookstore", schema = "")
@NamedQueries({
    @NamedQuery(name = "MasterInvoices.findAll", query = "SELECT m FROM MasterInvoices m")
    , @NamedQuery(name = "MasterInvoices.findByInvoiceId", query = "SELECT m FROM MasterInvoices m WHERE m.invoiceId = :invoiceId")
    , @NamedQuery(name = "MasterInvoices.findBySaleDate", query = "SELECT m FROM MasterInvoices m WHERE m.saleDate = :saleDate")
    , @NamedQuery(name = "MasterInvoices.findByNetValue", query = "SELECT m FROM MasterInvoices m WHERE m.netValue = :netValue")
    , @NamedQuery(name = "MasterInvoices.findByGrossValue", query = "SELECT m FROM MasterInvoices m WHERE m.grossValue = :grossValue")})
public class MasterInvoices implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "INVOICE_ID")
    private Integer invoiceId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SALE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date saleDate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "NET_VALUE")
    private BigDecimal netValue;
    @Basic(optional = false)
    @NotNull
    @Column(name = "GROSS_VALUE")
    private BigDecimal grossValue;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "invoiceId")
    private Collection<InvoiceDetails> invoiceDetailsCollection;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private Users userId;

    public MasterInvoices() {
    }

    public MasterInvoices(Integer invoiceId) {
        this.invoiceId = invoiceId;
    }

    public MasterInvoices(Integer invoiceId, Date saleDate, BigDecimal netValue, BigDecimal grossValue) {
        this.invoiceId = invoiceId;
        this.saleDate = saleDate;
        this.netValue = netValue;
        this.grossValue = grossValue;
    }

    public Integer getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Integer invoiceId) {
        this.invoiceId = invoiceId;
    }

    public Date getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(Date saleDate) {
        this.saleDate = saleDate;
    }

    public BigDecimal getNetValue() {
        return netValue;
    }

    public void setNetValue(BigDecimal netValue) {
        this.netValue = netValue;
    }

    public BigDecimal getGrossValue() {
        return grossValue;
    }

    public void setGrossValue(BigDecimal grossValue) {
        this.grossValue = grossValue;
    }

    public Collection<InvoiceDetails> getInvoiceDetailsCollection() {
        return invoiceDetailsCollection;
    }

    public void setInvoiceDetailsCollection(Collection<InvoiceDetails> invoiceDetailsCollection) {
        this.invoiceDetailsCollection = invoiceDetailsCollection;
    }

    public Users getUserId() {
        return userId;
    }

    public void setUserId(Users userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (invoiceId != null ? invoiceId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MasterInvoices)) {
            return false;
        }
        MasterInvoices other = (MasterInvoices) object;
        if ((this.invoiceId == null && other.invoiceId != null) || (this.invoiceId != null && !this.invoiceId.equals(other.invoiceId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.g4w18.MasterInvoices[ invoiceId=" + invoiceId + " ]";
    }
    
}
